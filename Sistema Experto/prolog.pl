iniciar:-write('Bienvenido').
hp:-write('Caso de Impresora HPDesckjet 2050').
inicio:-write('Saludos').

%Caso 1 Impresora

sol(conectar).
sol(habilitar).
sol(controlador).
sol(ip).
sol(cups).
sol(conexion).
sol(panel).
sol(sinconexion).
sol(solo).
sol(visualiza).


%Nodos del caso
imp(nove).
imp(ncon).
imp(conf).
imp(nhab).
imp(param).
imp(inalambrica).
imp(conectada).
imp(ruta).
imp(usb).
imp(sol):-write('s').
%USB
imp(sol1):-write('s1').
imp(sol2):-write('s2').
%Red
imp(sol3):-write('s3').
imp(sol4):-write('s4').
imp(sol5):-write('s5').
%Como esta conectado
imp(sol6):-write('s6').
imp(sol7):-write('s7').
imp(sol8):-write('s8').
imp(sol9):-write('s9').

%Hechos para el codigo
impresoraz(sol,conexion).
impresoraz(sol2,controlador).
impresoraz(sol3,conectar).
impresoraz(sol4,ip).
impresoraz(sol5,cups).
impresoraz(sol6,panel).
impresoraz(sol7,sinconexion).
impresoraz(sol8,solo).
impresoraz(sol9,visualiza).

impresora(ncon,conexion).
impresora(conf,controlador).
impresora(conf,conectar).
impresora(nhab,ip).
impresora(param,cups).
impresora(inalambrica,panel).
impresora(conectada,sinconexion).
impresora(ruta,solo).
impresora(usb,visualiza).

%lenguaje mas humano
conexion:-consulta(M,conexion).
controlador:-consulta(M,controlador).
conectar:-consulta(M,conectar).
ip:-consulta(M,ip).
cups:-consulta(M,cups).
panel:-consulta(M,panel).
sinconexion:-consulta(M,sinconexion).
solo:-consulta(M,solo).
visualiza:-consulta(M,visualiza).
%Logica del programa
%El usuario debe escrSbir consulta(X y Y sera lo que se desea consultar)
consulta(M,Y):-impresora(M,Y),impresoraz(X,Y),imp(X),!.

%Caso 2 tarjeta de red
tarjeta:-write('red inalambrica').

sl(no).
sl(paquete).
sl(terminal).
sl(terminal).
sl(repositorios).
sl(grafico).
sl(solo).
sl(descarga).
sl(sig).
sl(encuentra).
sl(noen).

red(sl):-write('sl').
red(sl1):-write('sl1').
red(sl2):-write('sl2').
red(sl3):-write('sl3').
red(sl4):-write('sl4').
red(sl5):-write('sl5').
red(sl6):-write('sl6').
red(sl7):-write('sl7').
red(sl8):-write('sl8').
red(sl9):-write('sl9').
red(sl10):-write('sl10').
red(tj).
red(busque).
red(consulte).
red(proc).
red(realizar).
red(continuacion).
red(gestor).
red(actividad).

tjrz(sl,no).
tjrz(sl5,si).
tjrz(sl1,paquete).
tjrz(sl2,terminal).
tjrz(sl3,repositorios).
tjrz(sl4,grafico).
tjrz(sl6,solo).
tjrz(sl7,descarga).
tjrz(sl8,sig).
tjrz(sl9,encuentra).
tjrz(sl10,noen).

tjr(tj,no).
tjr(tj,si).
tjr(no,paquete).
tjr(si,terminal).
tjr(consulte,repositorios).
tjr(busque,grafico).
tjr(proc,solo).
tjr(realizar,descarga).
tjr(continuacion,sig).
tjr(gestor,encuentra).
tjr(actividad,noen).

%lenguaje humano
no:-consultar(M,no).
si:-consultar(M,si).
paquete:-consultar(M,paquete).
terminal:-consultar(M,terminal).
repositorios:-consultar(M,repositorios).
grafico:-consultar(M,grafico).
individual:-consultar(M,solo).
descarga:-consultar(M,descarga).
siguiente:-consultar(M,sig).
encuentra:-consultar(M,encuentra).
noencuentra:-consultar(M,noen).


consultar(M,Y):-tjr(M,Y),tjrz(X,Y),red(X),!.

%Caso 3 Repositorio
%Palabras que el sistema no aceptara
repositorio:-write('Repositorio').

s(autentificado).
s(caducado).
s(cron).
s(prototipo).
s(denied).
s(almacenamiento).
s(gpg).

r(respuesta1):-write('r1').
r(respuesta2):-write('r2').
r(respuesta3):-write('r3').
r(respuesta4):-write('r4').
r(respuesta5):-write('r5').
r(respuesta6):-write('r6').
r(esta).
r(investigar).
r(error).
r(tentativo).
r(permission).
r(lleno).
r(llave).

rez(respuesta1,caducado).
rez(respuesta2,autentificado).
rez(respuesta3,cron).
rez(respuesta4,prototipo).
rez(respuesta5,denied).
rez(respuesta6,almacenamiento).
rez(respuesta2,gpg).

re(esta,caducado).
re(investigar,autentificado).
re(error,cron).
re(tentativo,prototipo).
re(permission,denied).
re(lleno,almacenamiento).
re(llave,gpg).

%Lenguaje humano
caducado:-preguntar(M,caducado).
autenticado:-preguntar(M,autentificado).
cron:-preguntar(M,cron).
comando:-preguntar(M,prototipo).
manualmente:-preguntar(M,prototipo).
ejecute:-preguntar(M,prototipo).
denied:-preguntar(M,denied).
permisologia:-preguntar(M,prototipo).
almacenamiento:-preguntar(M,almacenamiento).
espacio:-preguntar(M,prototipo).
gpg:-preguntar(M,gpg).

%Regla
preguntar(M,Y):-re(M,Y),rez(X,Y),r(X),!.

educativo:-write('no admitido').
contenido:-write('no admitido').
disco:-write('no admitido').
usuario:-write('no admitido').
grub:-write('no admitido').
bios:-write('no admitido').
ram:-write('no admitido').
memoria:-write('no admitido').
desbloquear:-write('no admitido').
actualizacion:-write('no admitido').
live:-write('no admitido').
vivo:-write('no admitido').
vit:-write('no admitido').
fecha:-write('no admitido').
hora:-write('no admitido').
sincronizacion:-write('no admitido').
caida:-write('no admitido').
caido:-write('no admitido').
plymouth:-write('no admitido').
pantalla:-write('no admitido').
monitor:-write('no admitido').
resolucion:-write('no admitido').
roto:-write('no admitido').
bluetooth:-write('no admitido').
booteo:-write('no admitido').
canaimita:-write('no admitido').
canaimitas:-write('no admitido').
internet:-write('no admitido').
teclado:-write('no admitido').
tablet:-write('no admitido').
epson:-write('no admitido').
cannon:-write('no admitido').
appel:-write('no admitido').
wine:-write('no admitido').
casio:-write('no admitido').
compaq:-write('no admitido').
hitachi:-write('no admitido').
generic:-write('no admitido').
generico:-write('no admitido').
sony:-write('no admitido').
dispositivo:-write('no admitido').
movil:-write('no admitido').
debian:-write('no admitido').
ubuntu:-write('no admitido').
windows:-write('no admitido').
videojuegos:-write('no admitido').
minecraft:-write('no admitido').
steam:-write('no admitido').
fornite:-write('no admitido').
android:-write('no admitido').
apple:-write('no admitido').
iphone:-write('no admitido').
nokia:-write('no admitido').
vetelca:-write('no admitido').
torrent:-write('no admitido').
bittorrent:-write('no admitido').
telegram:-write('no admitido').
skype:-write('no admitido').
google:-write('no admitido').
chrome:-write('no admitido').
sublime:-write('no admitido').
vim:-write('no admitido').
nano:-write('no admitido').
escaner:-write('no admitido').
fotocopiadora:-write('no admitido').
libreoffice:-write('no admitido').
writer:-write('no admitido').
calc:-write('no admitido').
impress:-write('no admitido').
gimp:-write('no admitido').
inskape:-write('no admitido').
evolution:-write('no admitido').
thunderbird:-write('no admitido').
fondo:-write('no admitido').
escritorio:-write('no admitido').
color:-write('no admitido').
barra:-write('no admitido').
sonido:-write('no admitido').
sesion:-write('no admitido').
touchpad:-write('no admitido').
